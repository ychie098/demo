data "aws_iam_policy" "ReadOnlyAccess"{
	arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}	
resource "aws_iam_user" "demo" {
  name = "demo-user1"

  tags = {
    Project = "demo"
  }
}

resource "aws_iam_access_key" "demo" {
  user = aws_iam_user.demo.name
}

resource "aws_iam_user_policy_attachment" "demo-policy" {
  user = aws_iam_user.demo.name
  policy_arn = "${data.aws_iam_policy.ReadOnlyAccess.arn}"
}

