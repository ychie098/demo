provider "aws" {
	region	="us-east-1"
}

resource "aws_vpc" "demo" {
	cidr_block	="192.168.0.0/16"
	instance_tenancy="default"
	tags = {
		Name = "demo"
		Project = "demo"
	}
}
resource "aws_internet_gateway" "demo" {
	vpc_id = "${aws_vpc.demo.id}"
	tags = {
			Name = "demo"
			Project = "demo"
 	}
}

resource "aws_subnet" "public_subnet" {
	vpc_id	="${aws_vpc.demo.id}"
	cidr_block="192.168.1.0/24"
	availability_zone = "us-east-1a"
	tags = {
		Name = "public_subnet"
		Project = "demo"
	}		
}
resource "aws_subnet" "private_subnet" {
	vpc_id	="${aws_vpc.demo.id}"
	cidr_block="192.168.2.0/24"
	availability_zone = "us-east-1a"
	tags = {
		Name = "private_subnet"
		Project = "demo"
	}		
}
/* Elastic IP for NAT */
resource "aws_eip" "demo" {
  vpc        = true
  depends_on = [aws_internet_gateway.demo]
}
/* NAT */
resource "aws_nat_gateway" "demo" {
  allocation_id = "${aws_eip.demo.id}"
  subnet_id     = "${aws_subnet.public_subnet.id}"
  depends_on    = [aws_internet_gateway.demo]
  tags = {
  		Name = "demo"
		Project = "demo"
		}
}
/* Routing table for public subnet */
resource "aws_route_table" "public_subnet" {
  	vpc_id ="${aws_vpc.demo.id}"
  	tags = {
			Name = "public_subnet"
    		Project = "demo"
  }
}
/* Routing table for private subnet */
resource "aws_route_table" "private_subnet" {
  	vpc_id ="${aws_vpc.demo.id}"
  	tags = {
			Name = "private_subnet"
    		Projecct = "demo"
  }
}
resource "aws_route" "public_internet_gateway" {
  	route_table_id = "${aws_route_table.public_subnet.id}"
  	destination_cidr_block = "0.0.0.0/0"
  	gateway_id = "${aws_internet_gateway.demo.id}"
}
resource "aws_route" "private_nat_gateway" {
  	route_table_id = "${aws_route_table.private_subnet.id}"
  	destination_cidr_block = "0.0.0.0/0"
  	gateway_id = "${aws_nat_gateway.demo.id}"
}
/* Route table associations */
resource "aws_route_table_association" "public" {
	subnet_id      = "${aws_subnet.public_subnet.id}"
	route_table_id = "${aws_route_table.public_subnet.id}"
}
resource "aws_route_table_association" "private" {
	subnet_id      = "${aws_subnet.private_subnet.id}"
	route_table_id = "${aws_route_table.private_subnet.id}"
}
/*==== VPC's Default Security Group ======*/
resource "aws_security_group" "vpc_demo_security" {
  name        = "vpc-demo-security"
  description = "Demo Security Group for VPC"
  vpc_id      = "${aws_vpc.demo.id}"
  depends_on  = [aws_vpc.demo]
/* inbound rules  */
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["118.189.0.0/16","116.206.0.0/16","223.25.0.0/16"]
  }

/* outbound rules */
  egress {
	protocol = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
    tags = {
    Name = "demo"
	Project = "demo"
	}
}