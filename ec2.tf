resource "tls_private_key" "demo_key_private" {
  algorithm = "RSA"
}
resource "aws_key_pair" "demo_key_public" {
  key_name   = "demo-key"
  public_key = "${tls_private_key.demo_key_private.public_key_openssh}"
	tags = {
    Name = "demo"
	Project = "demo"  }
}
resource "aws_instance" "demo" {
  ami           = "ami-042e8287309f5df03"
  key_name = "${aws_key_pair.demo_key_public.key_name}"
  instance_type = "t2.micro"
  availability_zone = "us-east-1a"
  vpc_security_group_ids =["${aws_security_group.vpc_demo_security.id}"]
  subnet_id = "${aws_subnet.private_subnet.id}"
  connection {
	user = "ubuntu"
	
	host = self.private_ip
	}
  user_data = <<-EOF
	#!/bin/bash
	sudo api-get update -y
	sudo api-get install -y docker.io -y
	sudo systemctl start docker.service
	sudo usermod -aG docker ubuntu
	docker pull nginx:1.18.0
	echo "<h1>Jayden Chan</h1>" | sudo tee /usr/share/nginx/html/index.html
	docker run -p 80:80 nginx:1.18.0
  EOF
	
  tags= {
    Name = "demo"
	Project = "demo"
  }
}
resource "aws_security_group" "demo_nlb" {
  name        = "demo_nlb"
  description = "Demo Security Group for NLB"
  vpc_id      = "${aws_vpc.demo.id}"
  depends_on  = [aws_vpc.demo]
/* inbound rules  */
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
/* outbound rules */
  egress {
	protocol = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
    tags = {
    Name = "demo"
	Project = "demo"
	}
}
resource "aws_lb" "demo" {
  name               = "demo-nlb"
  internal           = false
  load_balancer_type = "network"
  subnets = aws_subnet.public_subnet.*.id
  tags = {
    Name = "demo"
	Project = "demo"
	}
}
resource "aws_lb_target_group" "demo" {
  name     = "demo-tg"
  port     = 80
  protocol = "TCP"
  vpc_id   = "${aws_vpc.demo.id}"
}
resource "aws_lb_target_group_attachment" "demo" {
  target_group_arn = "${aws_lb_target_group.demo.arn}"
  target_id        = "${aws_instance.demo.id}"
  port             = 80
}
resource "aws_lb_listener" "demo" {
  load_balancer_arn = aws_lb.demo.arn
  port              = "80"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.demo.arn}"
  }
}